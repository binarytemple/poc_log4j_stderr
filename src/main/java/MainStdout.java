import org.apache.log4j.Logger;

public class MainStdout {
    public static void main(String[] args) {
        Logger.getLogger(MainStdout.class).info("info for stdout");
        Logger.getLogger(MainStdout.class).debug("debug for stdout");
    }
}
