import org.apache.log4j.Logger;

public class MainStderrStdout {

    public static void main(String[] args) {
        //Stderr destined stuff...
        Logger.getLogger(MainStdout.class).error("error for stderr");
        Logger.getLogger(MainStdout.class).warn("warn for stderr");

        //Stdout destined stuff...
        Logger.getLogger(MainStdout.class).info("info for stdout");
        Logger.getLogger(MainStdout.class).debug("debug for stdout");
    }
}
