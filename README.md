
Does it work?

Check that log WARN/ERROR are sent to stderr

mvn exec:java -Dexec.mainClass="MainStderr" > /dev/null

Check that log info/debug are sent to stdout (Maven output goes to stdout too)

mvn exec:java -Dexec.mainClass="MainStdout" 2> /dev/null

Run both


